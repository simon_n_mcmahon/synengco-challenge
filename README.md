synengco-challenge

**Note: Even though I believe it possible to look up the exact prices for December, I will neglect to do so. Treating the problem as if it were actually in the future, and adopting a training/testing set method instead.**

#Dataset Notes

Note the presence of a missing value inside the dataset at 2/10/2017 12:00. This appears to be the only unintentionally blank value however. It has been excluded from the sample/training/testing sets.

This decision was made as the missing values represents such a small part of the total dataset and we want to be able to use algorithms that need filled values.

There is the possibility of giving it the average results of its two neighbours if complete time series are needed but that will come from research.

#Research Notes.

## Way in which electricity works in Australia

Maximum price of $12,500 per kilowatt hour and minimum of negative $1,000 per kilowatt hour comma negative values representing where plant operators pay V Energy market to avoid shutting down. 

[Source](https://www.eex.gov.au/large-energy-users/energy-management/energy-procurement/energy-pricing/how-the-energy-market-operates)
These are regulated ceilings and floors but generally only reached a handful of times per year

Research the base load profile for generic year as will vary by season

Get external data for the other Queensland New South Wales connecta to see if they improve connector to see if they improve results

## Predicting spot price in electricity markets

[Good summary paper from Griffith](https://research-repository.griffith.edu.au/bitstream/handle/10072/38286/68786_1.pdf%3Bsequence=1) published in 2010.

[MODELING AND FORECASTING ELECTRICITY SPOT PRICES: A FUNCTIONAL DATA PERSPECTIVE By Dominik Liebl](https://arxiv.org/pdf/1310.1628.pdf) published in 2013.

[Forecasting spot electricity prices: Deep learning approaches and empirical comparison of traditional algorithms](https://www.sciencedirect.com/science/article/pii/S030626191830196X)
Published 2018. Compared 27 different algorithms including deep learning nets.

## Algorithm Selection and Comparison

The task is static, we are predicting a month in advance and so if the runtime is not seconds it does not matter. We may choose a longer running algorithm to get increased accuracy.

ROC curve to evaluate multiple algorithms on the testing set??
Not suitable as we are not using a binary classifier method, we are predicting a value.

We shall implement a few potential algoriths anad compare them.

The 2018 paper [here](https://www.sciencedirect.com/science/article/pii/S030626191830196X#s0115) compares algorithms (both statistical and machine learning using the sMAPE (Symmetric Mean Absolute Percent Error)). I will choose to do the same.
From the same source, there is also the possibility of doing statistical testing based on (5.6.1. Diebold-Mariano test) of "Day ahead" pricing.

### 2018 Paper Conclusions

> Considering the results listed in Table 8, we confirm the various observations made in Section 5.5.2:
>
> 1. The DNN, LSTM, and GRU models, i.e. 3 of the 4 proposed forecasters, are indeed statistically significantly better than the rest. In particular, the DNN shows a predictive accuracy that is statistically significantly better than the accuracy of all others. In addition, the LSTM and GRU models have an accuracy that is statistically significantly better than all others except the MLP.
>
> 2. Except for the fARX-based models, the accuracy of the machine learning methods is statistically significantly better than the accuracy of statistical methods.
>
> 3. Based on accuracy differences that are statistically significant, we can observe a very similar group separation pattern as the one described in Section 5.5.2.
>
> 4. The models with moving average terms have an accuracy that is statistically significantly worse than their AR counterparts.
>
> 5. The TBATS model has an accuracy that is statistically significantly better than any other model without exogenous inputs.
>
> 6. The accuracy of the SVR and hybrid-SVR models is not statistically significantly different.

Note that the results of this paper were for day aheadelectricity price forecasting. **Our problem is predicting into the medium term, a month's worth of projections**

Ruling out the Machine learning methods given in this paper, it was found that **fARX-type algorithms** gave the best result, even beating some of the machine learning approaches.

### Medium to Long Term predictions 

* [Probabilistic Mid- and Long-Term Electricity Price Forecasting](https://arxiv.org/pdf/1703.10806.pdf). Published in 2018.

	* **Tables 1-3 give a literature review of the probabistic and non-probabalistic models from the past 4 years for medium and long term forecasting.**

* [Midterm Electricity Market Clearing Price Forecasting Using Two-Stage Multiple Support Vector Machine](https://www.hindawi.com/journals/jen/2015/384528/) 2014.

	* Multiple SVN approach. Simple to implement.
	

# Decided course

Literature review revealed a lack of resources for mid-term projections. Based on the summary in Tables 1-3 [here](https://arxiv.org/pdf/1703.10806.pdf), the work from Yan and Chowdhury over 2013-2016 was selected due to it's progressive nature,
similiarity to the considered problem (1 month forecast with predicting off of 1 year of hourly existing data) and iterative nature. 

The 2016 course found that when comparing single layer SVM and LS-SVM (the chosen approach as hybrid models add much complexity to coding and this is a short term project), SVM is more suited to mid-term projections.

Algorithm is as follows.
1. Dataset is normalised before input
2. 2013: Fed into svm or ls-svm . Output
3. 2016: Grouped into price zones, fed into svm or ls-svm. Output

Chosing to use svm as shown with higher accuracy in 2017 analysis.

## Programming Libraries

For ls-svm, we can use [FukuML](https://pypi.org/project/FukuML/).

For svm, we can use [scikit-learn](http://scikit-learn.org/stable/modules/svm.html).

## Possible future work

* Use of hybrid SVM and ARMAX models as in Yan 2015.
* Use of two-stage SVM model using price zone analysis from Yan 2014.

## Body of work by Yan and Chowdhury
> * Yan, X. and Chowdhury, N. A. (2013). Mid-term electricity market clearing price forecasting: A hybrid LSSVM and ARMAX approach. International Journal of Electrical Power & Energy Systems, 53(0):20–26.
> * Yan, X. and Chowdhury, N. A. (2014). Mid-term electricity market clearing price forecasting: a multiple svm approach. International Journal of Electrical Power & Energy Systems, 58:206–214.
> * Yan, X. and Chowdhury, N. A. (2015). Midterm electricity market clearing price forecasting using two-stage multiple support vector machine. Journal of Energy, 2015.
> * Yan, X., Song, Y., and Chowdhury, N. A. (2016). Performance evaluation of single svm and lssvm based forecasting models using price zones analysis. In Power and Energy Engineering Conference (APPEEC), 2016 IEEE PES Asia-Pacific, pages 79–83. IEEE.



## Additional Dataset import notes

acquire the extra datasets.

daily price of natural gas (obtain) (done)
	got from http://nemweb.com.au/Reports/Current/GSH/Benchmark_Price/
previous year’s monthly average electricity MCP (obtain) (done)
	got from  https://www.aemo.com.au/Electricity/National-Electricity-Market-NEM/Data-dashboard
electricity daily peak demand (calculate) *** (done)
month (1 to 12) (calculate) (done)
hour of the day (1 to 24). (calculate) (done)

